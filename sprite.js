console.log("linked");


//purpose: just draw everything in the beginning 
window.onload = function() {
    console.log("in function");
    drawFruit(300, 300);
    drawAnimal(300, 300);
    drawVehicle(200, 700);
    drawPlant(500, 500);

  
}

//function should only do 1 thing!!
function drawFruit(x, y){
    console.log("drawing fruit");
var str = "<svg><g transform='translate(45 45)'><circle cx='50' cy='50' r='40' fill='orange' /><rect width='10' height='10' x='45' y='2' fill='green' /></g></svg><br>"

  document.getElementById("fruit").innerHTML = str;

}


function drawAnimal(x, y){
     console.log("drawing animal")

    var str2 = "<svg height='300' width='300'><g><circle cx='150' cy='150' r='40' fill='white' stroke='black' stroke-width='2' /><circle cx='110' cy='110' r='20' fill='black' stroke='black' stroke-width='2' /><circle cx='190' cy='110' r='20' fill='black' stroke='black' stroke-width='2' /><circle cx='150' cy='150' r='2' fill='black' stroke='black' stroke-width='2' /><circle cx='160' cy='130' r='5' fill='black' stroke='black' stroke-width='2' /><circle cx='140' cy='130' r='5' fill='black' stroke='black' stroke-width='2' /></g></svg><br>"

    document.getElementById("animal").innerHTML = str2;
}

function drawVehicle(x, y){
    console.log("drawing Vehicle")

    var str3 = "<svg width='400' height='400'><g><rect width='300' height='100' x='200' y='200' fill='grey'/><rect width='100' height='100' x='180' y='150' fill='grey' /><circle cx='220' cy='320' r='30' fill='black' /><circle cx='350' cy='320' r='30' fill='black' /></g></svg><br>"

    document.getElementById("vehicle").innerHTML = str3

}

function drawPlant(x, y){
    console.log("drawing Plant")
    
    var str4 = "<svg height='500' width='500'><g><polygon points='100,10 40,198 190,78 10,78 160,198' fill='pink' stroke='yellow' stroke-width='3' /><line x1='100' y1='150' x2='100' y2='300' stroke='green' stroke-width='2' /></g></svg><br>"

    document.getElementById("plant").innerHTML = str4

}


"use strict";
window.addEventListener("load", function (ev) {
    // SVG UI for Tetris
    var opt = {
        svgid: "view",
        stageid: "stage",
        svgns: "http://www.w3.org/2000/svg",
        scale: 24,
        width: 10,
        height: 20,
        cursor: 50,
        input: 200,
        fall: 3,
    };
    
    var newBlock = function () {
        return Tetris.Block(
            0|(opt.width / 2) - 2, 0, 0, 
            Tetris.shapes[0|(Math.random() * Tetris.shapes.length)]);
    };
    
    var render = function () {
        while (view.hasChildNodes()) view.removeChild(view.lastChild);
        stage.eachStone(function (x, y, color) {
            view.appendChild(stone(x, y, color));
        });
        block.eachStone(function (x, y, color) {
            view.appendChild(stone(x, y, color));
        });
    };
    
    var stone = function (x, y, color) {
        var rect = document.createElementNS(opt.svgns, "rect");
        rect.setAttribute("x", opt.scale * x);
        rect.setAttribute("y", opt.scale * y);
        rect.setAttribute("width", opt.scale);
        rect.setAttribute("height", opt.scale);
        rect.setAttribute("fill", color);
        rect.setAttribute("stroke", "black");
        return rect;
    };
    
    var tryLeft = function () {
        var next = block.left();
        if (!next.ok(stage)) return;
        block = next;
        render();
    };
    var tryRight = function () {
        var next = block.right();
        if (!next.ok(stage)) return;
        block = next;
        render();
    };
    var tryRotate = function () {
        var next = block.rotate();
        if (!next.ok(stage)) return;
        block = next;
        render();
    };
    var tryFall = function () {
        var next = block.fall();
        if (next.ok(stage)) {
            block = next;
            return render();
        } else {
            block.put(stage);
            render();
            setTimeout(function () {
                stage.shrink();
                block = newBlock();
                if (!block.ok(stage)) {
                    // gameover
                    stage.reset();
                }
                render();
            }, 10);
        }
    };
    
    // init
    var svg = document.getElementById(opt.svgid);
    svg.setAttribute("width", opt.scale * opt.width);
    svg.setAttribute("height", opt.scale * opt.height);
    svg.style.backgroundColor = "grey";
    var view = document.getElementById(opt.stageid);

    var stage = Tetris.Stage(opt.width, opt.height);
    var block = newBlock();

    //document.body.addEventListener("keydown", keyHandler, false);
    render();
    var cursor = SVGCursor(svg, opt.cursor);
    var count = 0;
    var loop = window.setInterval(function () {
        count = (count + 1) % opt.fall;
        if (cursor.move.y < -0.5) tryRotate();
        if (cursor.move.x < -0.5) tryLeft();
        if (cursor.move.x > 0.5) tryRight();
        if (cursor.move.y > 0.5) tryFall();
        if (count === 0) tryFall(); 
    }, opt.input);
}, false);

