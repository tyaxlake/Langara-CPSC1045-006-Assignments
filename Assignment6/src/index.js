//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    let count = 0;
    count = count + containers[0].innerHTML.length;
    containers[0].innerHTML = containers[0].innerHTML.toUpperCase();
    count = count + containers[1].innerHTML.length;
    containers[1].innerHTML = containers[1].innerHTML.toUpperCase();
    count = count + containers[2].innerHTML.length;
    containers[2].innerHTML = containers[2].innerHTML.toUpperCase();
    count = count + containers[3].innerHTML.length;
    containers[3].innerHTML = containers[3].innerHTML.toUpperCase();
    count = count + containers[4].innerHTML.length;
    containers[4].innerHTML = containers[4].innerHTML.toUpperCase();
    count = count + containers[5].innerHTML.length;
    containers[5].innerHTML = containers[5].innerHTML.toUpperCase();
    count = count + containers[6].innerHTML.length;
    containers[6].innerHTML = containers[6].innerHTML.toUpperCase();
    count = count + containers[7].innerHTML.length;
    containers[7].innerHTML = containers[7].innerHTML.toUpperCase();

    for (let i=0; i< containers.length; i++){
           count = count + containers[i].innerHTML.length; 
    containers[i].innerHTML = upper(containers[i]);

    }


    //Output Part 1: You do not need to change this.
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0;
    for (i = heights.length-1; i >= 0; i -= 1){
    x = x + 10;
    y = 200 - heights[i] * 20;
    pointString = stringmaker(pointString, x, y);

    }
    //Replace the following code with a for/of loop.
    //In this loop, call a function that you will write to 
    //help create the string of points

    x = x + 10;
    y = 200 - heights[0] * 20;
    //Take the next line of code and make it a function
    //You function will take in, x, y, and the existing string ( 3 parameters into total)
    //and return the new string with the ponts added.
    //Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[1] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[2] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[3] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[4] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[5] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[6] * 20;
    pointString = pointString + x + "," + y + " ";
    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    for (i=0; i < 4; i += 1){
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    x= 0;
    let circleString = [i];
    circleString = stringcricle(circleString, cx, cy, r) 
    }

    x = data[0];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[1];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[2];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[3];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    svgString += "</svg>";
    let output3 = document.querySelector("#output3");
    output3.innerHTML += svgString;
}

function upper(element){
    return element.innerHTML.toUpperCase();

}

function stringmaker(stringpoint, x, y){
    return stringpoint + x +  "," + y + " ";
}




function stringcircle(circleString, cx, cy, r){
    return stringcircle + cx + "," + circleString + "," + cy + "," + r + " ";
}





function popPage(){
var initialSize = document.getElementById("boxs").value;
var noBoxes = document.getElementById("boxn").value;
var growth = document.getElementById("boxg").value;
var hw = parseInt(initialSize);
var myanswer="";

for(var i = 0; i < noBoxes; i++){
    myanswer += "<svg width='400' height='400'><rect x='50' y='20' rx='20' ry='20' width='"+hw+"' height='"+hw+"' style='fill:blue;stroke:black;stroke-width:2;opacity:0.5' /></svg><br>";
    hw+=parseInt(growth);
    console.log(hw);
}

document.getElementById("answer").innerHTML = myanswer;
}



