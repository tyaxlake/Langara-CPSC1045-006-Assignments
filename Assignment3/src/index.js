//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function sliderXlistener(){
    let sliderX = document.querySelector("#rangeX");
    let circle = document.querySelector("#circle");
    circle.setAttribute("r", sliderX.value);
    console.log(circle); 
}

function eyeballYlistener(){
    let eyeballY = document.getElementById("eyeY");
    let eye1 = document.getElementById("eye1");
    eye1.setAttribute("cx", eyeballY.value);

    console.log(eye1);

}

function eyeballXlistener(){
    let eyeballX = document.getElementById("eyeX");
    let eye2 = document.getElementById("eye2");
    eye2.setAttribute("cx", eyeballX.value);

    console.log(eye2);
}



function changeColor(){
    let colorPicker = document.querySelector("#color");
    let hair = document.querySelector("#hair");
    hair.setAttribute("fill", colorPicker.value);
}


function lengthXlistener(){
    let lengthX = document.querySelector("#triX");
    let hair = document.getElementById("hair");
    

    let points = "200 20, 250 "+(350+  Number(lengthX.value))+" ,350 200,400 250,500 200,600 350,700 200,";
    hair.setAttribute("points", points);
    console.log(hair);
}

var rotation = 0;

function rotatex() {
    rotation +=180;
    var mouth = document.getElementById("mouth");
    let transFormString = "rotate(" + rotation + " 400 500)"
    mouth.setAttribute("transform", transFormString);
}



//document.getElementById("eyeY").addEventListener("click", eyeballYlistener);

