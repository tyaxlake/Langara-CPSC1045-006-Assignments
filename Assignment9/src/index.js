//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

//make a blue print 
class Definition {

    constructor(w, d){
        this.word = w;
        this.def = d;
    }

    // //getters 
    // get word() {
    //     return this._word;
    // }
    // get definition() {
    //     return this._def;    
    // }
    // set words(w){
    //     this._word = w;
    // }
    // set definition(d){
    //     this._definition = d;
    // }
    // // methods 
    // compareWord(w){
    //     return this._word === w;
    // }
}


//create an object 
let dictionary = [];
let word1 = new Definition("cat", "Ninjas in fur suit with knives hidden in the paws");
let word2 = new Definition("dog", "A person as unpleasant, contemptible, or wicked");
let word3 = new Definition("door", "A device to walk through walls");
let word4 = new Definition("windows", "A PC based operation system, by Microsoft");
let word5 = new Definition("car", "2-ton steel carriages powered by explosions, made out of dinosaurs which are used for transportation");
//.. after you create all the words 
dictionary.push(word1, word2, word3, word4, word5);

    function submit() {
    let userInput = document.querySelector("#wordbank").value  ;

    let message = "not found";
    for (var i = 0; i < dictionary.length ; i++)
    {
        if (userInput == dictionary[i].word){
            message = dictionary[i].def;
            break;
        }
    }


    document.querySelector("#output").innerHTML = message;

    }

